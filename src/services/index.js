const { fetchSiteMenuService } = require('./fetch-site-menu-service');

module.exports = {
  fetchSiteMenuService,
};
