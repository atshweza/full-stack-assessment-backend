const { fetchSiteMenuClient } = require('../../clients');
const { handleResponse } = require('../../utils/helpers');

async function fetchSiteMenuService() {
  const { success, data, error } = await fetchSiteMenuClient();

  return handleResponse({ success, data, error });
}

module.exports = { fetchSiteMenuService };
