const { fetchSiteMenuController } = require('./fetch-site-menu-controller');

module.exports = {
  fetchSiteMenuController,
};
