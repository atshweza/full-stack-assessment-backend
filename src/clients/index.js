const { fetchSiteMenuClient } = require('./fetch-site-menu-client');

module.exports = {
  fetchSiteMenuClient,
};
