async function fetchSiteMenuClient() {
  return {
    success: true,
    data: {
      name: 'betway',
      color: '#00a826',
      logoUrl: 'https://betway.com/doc-centre/assets/betway-logo-white-sml.png',
      landingPageImage:'https://cdn.betwaygroup.com/medusa-production-cache/b/6/b604ec0b6b3e584899a17fb3255e5177a8e649e0.webp',
      menuList: [
        {
          title: 'sports',
        },
        {
          title: 'casino',
        },
        {
          title: 'live & real',
        },
        {
          title: 'eSports',
        },
        {
          title: 'vegas',
        },
      ],
      promtion: {
        offerSubHeader: 'Sport new cutormer offer',
        offerHeader: 'Get up to £10 in Bets',
        JoinLabel: 'Join Now',
      },
      loginLabel: 'Login',
      signupLable: 'Sign up',
    },
    error: false,
  };
}

module.exports = { fetchSiteMenuClient };
